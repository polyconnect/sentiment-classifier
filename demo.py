import time
from datetime import datetime

import joblib
from flask import Flask, render_template, request

from sentiment_classifier import SentimentClassifier

app = Flask(__name__)

start_time = time.time()
classifier = SentimentClassifier()


def timestamp():
    return datetime.now().strftime("%Y-%m-%d-%H-%M-%S")


cls_dict = {0: "negative", 1: "positive", -1: "error"}


@app.route("/", methods=["POST", "GET"])
def index_page(text="", prediction_message="", color_class=""):
    if request.method == "POST":
        text = request.form["text"]
        prediction_message, pred_class = classifier.get_predictions(text)
        color_class = cls_dict.get(pred_class, "error")

    return render_template(
        "index.html",
        text=text,
        prediction_message=prediction_message,
        color_class=color_class,
    )


@app.route("/testset", methods=["GET"])
def test_set_page():
    test_data = joblib.load("test_data.pkl")
    test_data = tuple(
        ("+" if label else "-", review, "positive" if label else "negative")
        for label, review in test_data
    )
    return render_template("testset.html", reviews=test_data)
