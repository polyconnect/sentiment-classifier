import joblib
import nltk
import numpy as np
from nltk import word_tokenize

nltk.download("punkt")


class SentimentClassifier(object):
    def __init__(self):
        self.model = joblib.load("model.pkl")
        self.cls_dict = {0: "отрицательный", 1: "положительный"}

    @staticmethod
    def get_certainty(probability) -> str:
        if probability < 0.55:
            return "возможно"
        if probability < 0.7:
            return "вероятно"
        if probability < 0.95:
            return "скорее всего"
        else:
            return "определённо"

    def predict_list(self, texts) -> tuple[np.array, np.array]:
        return self.model.predict(texts), self.model.predict_proba(texts)

    def get_predictions(self, review: str) -> tuple[str, int]:
        sentence = " ".join(
            filter(lambda x: x.isalnum(), word_tokenize(review))
        ).lower()
        try:
            pred_class, proba = (
                self.model.predict([sentence])[0],
                self.model.predict_proba([sentence])[0],
            )
        except Exception:
            return "Ошибка", -1
        return (
            f"{self.get_certainty(proba.max())} {self.cls_dict[pred_class]}",
            pred_class,
        )
